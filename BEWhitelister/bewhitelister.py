import discord
from fabric import Connection
from redbot.core import commands, Config, checks


class BEWhitelister(commands.Cog):
    def __init__(self, bot, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.bot = bot
        self.config = Config.get_conf(self, identifier=9834254160)
        default_global = {
            "waitTimeArg": 5,
            "messagesBeforeWhitelistArg": 20,
            "counterTimeoutArg": 2,
        }
        self.config.register_global(**default_global)

    @commands.group()
    @checks.guildowner_or_permissions(administrator=True)
    async def bewhitelisterset(self, ctx: commands.Context):
        """Manage BEWhitelister options."""

    @bewhitelisterset.command()
    async def channel(self, ctx: commands.Context, channel: discord.TextChannel = None):
        """Set the channel that is checked for usernames.
        Leave empty to use the channel that the command is used in.
        """
        guild = ctx.guild
        if channel:
            await self.config.guild(guild).whitelist_channel.set(channel.id)
            await ctx.send(
                "I will now look in {channel} for usernames.".format(channel=channel.mention)
            )
        else:
            await self.config.guild(guild).warn_channel.set(channel)
            await ctx.send("I will now look in this channel for usernames.")

    @bewhitelisterset.command()
    async def hostname(self, ctx: commands.Context, host: str):
        """Set the SSH host location."""
        await self.config.hostnameArg.set(str(host))
        await ctx.send(f"Hostname is now set to {await self.config.hostnameArg()}")

    @bewhitelisterset.command()
    async def port(self, ctx: commands.Context, port: int):
        """Set the SSH port."""
        await self.config.portArg.set(port)
        await ctx.send(
            f"SSH port is now set to {await self.config.portArg()}"
        )

    @bewhitelisterset.command()
    async def user(self, ctx: commands.Context, user: str):
        """Set the SSH username."""
        await self.config.userArg.set(user)
        await ctx.send(
            f"Username is now set to {await self.config.userArg()}"
        )

    @bewhitelisterset.command()
    async def password(self, ctx: commands.Context, password: str):
        """Set the SSH password."""
        await self.config.passwordArg.set(password)
        await ctx.send(
            f"Password is now set."
        )

    @commands.group()
    @checks.guildowner_or_permissions(ban_members=True)
    @commands.cooldown(rate=1, per=15, type=commands.BucketType.default)  # I don't understand what BucketType is
    async def bewhitelister(self, *args):
        """Manage the BE whitelist manually."""

    @bewhitelister.command()
    async def add(self, ctx, *args):
        """Add usernames to the whitelist"""
        username = " ".join(args[:])
        async with ctx.typing():
            c = Connection(host=await self.config.hostnameArg(),
                           port=await self.config.portArg(),
                           user=await self.config.userArg(),
                           connect_kwargs={'password': await self.config.passwordArg()})
        cmd = f'USERNAME="{username}" ./whitelistadd.sh'
        response = c.run(cmd)
        c.close()  # move this below the if statement when a double check sequence is added
        if "Player already in whitelist" in response.stdout or "Player added to whitelist" in response.stdout:
            await ctx.send(response.stdout)
        else:
            await ctx.send(f'''A wacky thing happened? Please show the below error to someone.
{response.stdout}''')

    @bewhitelister.command()
    async def remove(self, ctx, *args):
        """Remove usernames to the whitelist"""
        username = " ".join(args[:])
        async with ctx.typing():
            c = Connection(host=await self.config.hostnameArg(),
                           port=await self.config.portArg(),
                           user=await self.config.userArg(),
                           connect_kwargs={'password': await self.config.passwordArg()})
        cmd = f'USERNAME="{username}" ./whitelistdel.sh'
        response = c.run(cmd)
        c.close()  # move this below the if statement when a double check sequence is added
        if 'Player removed from whitelist' in response.stdout:
            await ctx.send(response.stdout)
        else:
            await ctx.send(f'''A wacky thing happened? Please show the below error to someone.
{response.stdout}''')
